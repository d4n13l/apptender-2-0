﻿----select * from AspNetRoles
----select * from AspNetUsers
----select * from AspNetUserRoles
----select * from Estado
----select * from TipoDocumento
--Drop DATABASE AppTender
--CREATE DATABASE AppTender

CREATE TABLE Bar (
	IdBar int NOT NULL PRIMARY KEY identity,
	Nombre varchar (20) NOT NULL
);

CREATE TABLE Sede (
	IdSede int NOT NULL PRIMARY KEY IDENTITY,
	Descripcion varchar (20) NOT NULL,
	CodigoCiudad varchar (20) NOT NULL,
	IdBar INT NOT NULL,
	constraint sedetobar foreign key (IdBar) references Bar(IdBar)
);

CREATE TABLE Estado (
	IdEstado int NOT NULL PRIMARY KEY identity,
	Descripcion varchar (20) NOT NULL
);
CREATE TABLE TipoDocumento(
	IdTipoDocumento int NOT NULL PRIMARY KEY identity,
	CodigoTexto varchar (3) NOT NULL,
	Descripcion varchar (50) NOT NULL
);

CREATE TABLE Roles 
(
	IdRol int NOT NULL PRIMARY KEY identity,
	Descripcion  VARCHAR (20) NOT NULL
);

CREATE TABLE Usuario (
    IdUsuario int NOT NULL PRIMARY KEY identity,
    NumeroDocumento varchar (15) NOT NULL unique,
    Nombres varchar (40) NOT NULL,
    Apellidos varchar (40) NULL,
    FechaActualizacion datetime NULL,
	Password varchar(50) not null,
    IdUsuarioActualizacion int NULL,
    IdTipoDocumento int NOT NULL,
    IdEstado int NOT NULL,
	IdRol INT NOT NULL,
    IdSede INT NOT NULL,
	IdBar Int NOT NULL,
    constraint usuarioTObar foreign key (IdBar) references Bar(IdBar),
    constraint usuarioTOsede foreign key (IdSede) references Sede(IdSede),
	constraint usuarioTOtipodoc foreign key (IdTipoDocumento) references TipoDocumento(IdTipoDocumento),
	constraint usuarioTOestado foreign key (IdEstado) references Estado(IdEstado),
	constraint usuarioTOrol foreign key (IdRol) references Roles(IdRol)
);

CREATE TABLE TipoProducto (
    IdTipoProducto int NOT NULL PRIMARY KEY identity,
    Descripcion varchar (50) NOT NULL,
    FechaActualizacion datetime NULL,
    IdUsuarioActualizacion int NULL,
	IdSede INT NOT NULL,
	constraint TipoproductoTOsede foreign key (IdSede) references Sede(IdSede),
    CONSTRAINT TipoproductoTOusuario FOREIGN KEY (IdUsuarioActualizacion) REFERENCES Usuario (IdUsuario)
);

CREATE TABLE Marca (
    IdMarca int NOT NULL PRIMARY KEY identity,
    Descripcion varchar (45) NOT NULL,
    FechaActualizacion datetime NULL,
    IdUsuarioActualizacion int NULL,
	IdSede INT NOT NULL,
	IdTipoProducto int NOT NULL,
	CONSTRAINT marcaTOtipo FOREIGN KEY (IdTipoProducto) REFERENCES TipoProducto (IdTipoProducto),
	constraint marcaTOsede foreign key (IdSede) references Sede(IdSede),
    CONSTRAINT marcaTOusuario FOREIGN KEY (IdUsuarioActualizacion) REFERENCES Usuario (IdUsuario)
);

CREATE TABLE Presentacion (
    IdPresentacion int NOT NULL PRIMARY KEY identity,
    Descripcion varchar (45) NOT NULL,
    FechaActualizacion datetime NULL,
    IdUsuarioActualizacion int NULL,
	IdSede INT NOT NULL,
	IdTipoProducto int NOT NULL,
	IdMarca int NOT NULL,
	constraint presentacionTOsede foreign key (IdSede) references Sede(IdSede),
    CONSTRAINT presentacionTOusuario FOREIGN KEY (IdUsuarioActualizacion) REFERENCES Usuario (IdUsuario),
	CONSTRAINT PresentacionTOtipo FOREIGN KEY (IdTipoProducto) REFERENCES TipoProducto (IdTipoProducto),
	CONSTRAINT PresentacionTOmarca FOREIGN KEY (IdMarca) REFERENCES Marca (IdMarca)
);
	
CREATE TABLE Tamano (
    IdTamano int NOT NULL PRIMARY KEY identity,
    Descripcion varchar (45) NOT NULL,
    FechaActualizacion datetime NULL,
    IdUsuarioActualizacion int NULL,
	IdSede INT NOT NULL,
	IdTipoProducto int NOT NULL,
	IdMarca int NOT NULL,
	constraint TamanoTOsede foreign key (IdSede) references Sede(IdSede),
    CONSTRAINT TamanoTOusuario FOREIGN KEY (IdUsuarioActualizacion) REFERENCES Usuario (IdUsuario),
	CONSTRAINT TamanoTOtipo FOREIGN KEY (IdTipoProducto) REFERENCES TipoProducto (IdTipoProducto),
	CONSTRAINT TamanoTOmarca FOREIGN KEY (IdMarca) REFERENCES Marca (IdMarca)

);

CREATE TABLE Producto (
    IdProducto int NOT NULL PRIMARY KEY identity,
    IdTipoProducto int NOT NULL,
	IdMarca int NOT NULL,
	IdPresentacion INT NULL,
	IdTamano INT NULL,
	Precio INT NOT NULL,
	Cantidad INT NOT NULL, 
    FechaActualizacion datetime NULL,
    IdUsuarioActualizacion int NULL,
	IdSede INT NOT NULL,	
	IdEstado INT NOT NULL,
	CONSTRAINT ProductoTOEstado FOREIGN KEY (IdEstado) REFERENCES Estado (IdEstado),
	constraint productoTOsede foreign key (IdSede) references Sede(IdSede),
    CONSTRAINT ProductoTOtipoProducto FOREIGN KEY (IdTipoProducto) REFERENCES TipoProducto (IdTipoProducto),
    CONSTRAINT ProductoTOmarca FOREIGN KEY (IdMarca) REFERENCES Marca (IdMarca),
    CONSTRAINT ProductoTOpresentacion FOREIGN KEY (IdPresentacion) REFERENCES Presentacion (IdPresentacion),
	CONSTRAINT ProductoTOtamano FOREIGN KEY (IdTamano) REFERENCES Tamano (IdTamano),
    CONSTRAINT ProductoTOusuario FOREIGN KEY (IdUsuarioActualizacion) REFERENCES Usuario (IdUsuario)
);

CREATE TABLE Pedido (
    IdPedido int NOT NULL PRIMARY KEY IDENTITY,
	Cantidad int null,
    Valor int NOT NULL,
	IdProducto int null,
    IdEstado int NOT NULL,
	IdUsuario int NOT NULL,
	IdSede INT NOT NULL,		
	constraint pedidoTOsede foreign key (IdSede) references Sede(IdSede),
    CONSTRAINT pedidoTOusuarioMesa FOREIGN KEY (IdUsuario) REFERENCES Usuario (IdUsuario),
    CONSTRAINT pedidoTOestado FOREIGN KEY (IdEstado) REFERENCES Estado (IdEstado),
	CONSTRAINT pedidoTOproducto FOREIGN KEY (IdProducto) REFERENCES Producto (IdProducto)
);

CREATE TABLE Pago (
    IdPago int NOT NULL PRIMARY KEY identity,
    FechaHora int NOT NULL,
    ValorPago int NOT NULL,
    IdUsuarioRegistro int NOT NULL,
    IdPedido int NOT NULL,
	IdSede INT NOT NULL,
	IdMesa INT NOT NULL,
	constraint pagoTOsede foreign key (IdSede) references Sede(IdSede),
    CONSTRAINT pagoTOusuarioMesa FOREIGN KEY (IdMesa) REFERENCES Usuario (IdUsuario),
	CONSTRAINT pagoTOusuarioUsuario FOREIGN KEY (IdUsuarioRegistro) REFERENCES Usuario (IdUsuario),
    CONSTRAINT pagoTOpedido FOREIGN KEY (IdPedido) REFERENCES Pedido (IdPedido)
);